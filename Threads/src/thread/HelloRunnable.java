package thread;

public class HelloRunnable implements Runnable  {
	
	private long delay;
	private int number;
	public HelloRunnable(long delay, int number) {
	
		this.delay = delay;
		this.number = number;
	}
	
	@Override
	public void run() {
		
		
		for (var i = 0; i < this.number; i++) {
			System.out.println("Current Thread Name- " + Thread.currentThread().getName());
			try {
	            Thread.sleep(this.delay );
	        } catch (InterruptedException e) {
	            e.printStackTrace();
	        }}
	}

}
