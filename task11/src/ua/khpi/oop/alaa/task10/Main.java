package ua.khpi.oop.alaa.task10;

import java.util.concurrent.ThreadLocalRandom;

public class Main {

	public static void main(String[] args) {
		PrintMessageTask task = new PrintMessageTask("abc", 1000, 5);
		PrintMessageTask task1 = new PrintMessageTask("abc", 1000, 5);

		long startTime = System.currentTimeMillis();
		task.run();
		long elapsedMillis = System.currentTimeMillis() - startTime;
		System.out.println("Time: " + elapsedMillis);

		Thread thread = new Thread(task, "New Thread");
		Thread thread2 = new Thread(task1, "New Thread 2");

		thread.start();
		thread2.start();
		System.out.println("Time: " + elapsedMillis);
		PrintMessageTask tasks[] = new PrintMessageTask[20];
		Thread threads[] = new Thread[20];
		for (int i = 0; i < tasks.length; i++) 
		{
			
			tasks[i] = new PrintMessageTask("abc", Math.abs(ThreadLocalRandom.current().nextInt() % 5000),
					Math.abs(ThreadLocalRandom.current().nextInt() % 10));
			threads[i] = new Thread(tasks[i]);
			threads[i].start();
			if(elapsedMillis >=10000) {
				threads[i].interrupt();
				System.out.println("Time: " + elapsedMillis);
				}

		}
		
		

	}

}
