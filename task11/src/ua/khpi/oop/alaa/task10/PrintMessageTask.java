package ua.khpi.oop.alaa.task10;

public class PrintMessageTask implements Runnable {
	private String text;
	private long delay;
	private int number;

	public PrintMessageTask(String text, long delay, int number) {
		this.text = text;
		this.delay = delay;
		this.number = number;
	}

	@Override
	public void run() {
		for (var i = 0; i < this.number; i++) {
		System.out.println(this.text);
		try {
            Thread.sleep(this.delay );
        } catch (InterruptedException e) {
            e.printStackTrace();
        }}
		
	}
}
