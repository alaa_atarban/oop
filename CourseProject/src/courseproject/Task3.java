package courseproject;

import java.util.Scanner;

public class Task3 {
private static Scanner predicate;
	
	public static void main(String[] args) {
		predicate = new Scanner(System.in);
		System.out.println("Input two predicates :");
		String P = predicate.nextLine();
		StringBuilder S = new StringBuilder(P);      
		
		// Let  X = {x1, x2},  Y={y1, y2}
		Boolean[] Px1 = {false, false, false, false, false, false, false, false,true,  true, true,  true, true,  true,  true, true};
		Boolean[] Px2 = {false, false, false, false, true,  true,  true,  true, false, false,false, false,true,  true,  true, true};
		Boolean[] Py1 = {false, false, true,  true,  false, false, true,  true, false, false,true,  true, false, false, true, true};
		Boolean[] Py2 = {false, true,  false, true,  false, true,  false, true, false, true, false, true, false, true,  false,true};
		Boolean left = null, right=null, result = false;
		int op = S.lastIndexOf("(")+3;
		
		int i = 0;
		// '\u2203' is unicode for ∃     (Exist)
		// '\u2200' is unicode for ∀ (for all)
			while(i < 16 && result==false) {		
				
			if(S.charAt(0) == '\u2200' && S.charAt(2) == '\u2203' && S.charAt(op) == '\u2228') {  
				left = (Px1[i] || Py1[i])&&(Px1[i] || Py2[i]);           
				right = (Px2[i] || Py1[i])&&(Px2[i] || Py2[i]);
				result = left && right;
			}
			if(S.charAt(0) == '\u2203' && S.charAt(2) == '\u2200'&& S.charAt(op) == '\u2228') {
				left = (Px1[i] || Py1[i])&&(Px1[i] || Py2[i]);
				right = (Px2[i] || Py1[i])&&(Px2[i] || Py2[i]);
				result = left || right;
			}
			if(S.charAt(0) == '\u2200' && S.charAt(2) == '\u2203' && S.charAt(op) == '\u2227') {  
				left = (Px1[i] && Py1[i])&&(Px1[i] && Py2[i]);           
				right = (Px2[i] && Py1[i])&&(Px2[i] && Py2[i]);
				result = left && right;	
			}
			if(S.charAt(0) == '\u2203' && S.charAt(2) == '\u2200'&& S.charAt(op) == '\u2227') {
				left = (Px1[i] && Py1[i])&&(Px1[i] && Py2[i]);
				right = (Px2[i] && Py1[i])&&(Px2[i] && Py2[i]);
				result = left || right;
			
			
				
			}
			      /// \u2228 is unicode for logical OR (∨)
			
			     /// \u2227 is unicode for logical AND (∧)
		if (result==true) {
			System.out.println("The predicate is satisfied");
			System.out.println("Px1 =" + Px1[i] + " |Px2 =" + Px2[i] + "| Py1= " + Py1[i] + "| Py2= " + Py2[i]);
		}
			i++;
		}
			if(result == false) {
					System.out.println("The predicate is Not satisfied");
			
			}
	}

}
