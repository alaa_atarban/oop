package ua.khpi.oop.alaa.task08;

public interface Sellable {
	double getCost();

	public static Sellable cheapest(Sellable[] items) {
		Sellable cheapestItem = items[0];
		for(Sellable s : items) {
			if(cheapestItem.getCost()>s.getCost())
				cheapestItem = s;
		} 
			
			
		
	
		return cheapestItem;
	}

	public static double totalCost(Sellable[] items) {
		double total = 0;
		for(Sellable s : items) {
			total = total + s.getCost();
		}
		
		return total;
	}
	public static void SortAndPrint(Sellable[] items) {
		Sellable temp;
	
		for(int i =0; i<items.length;i++) 
		{
			 for (int j = i + 1; j < items.length; j++) 
	            {
	                if (items[i].getCost()< items[j].getCost()) 
	                	{
	                    temp = items[i];
	                    items[i] = items[j];
	                    items[j] = temp;
	                	}
	                }
		
		
		
		}
		 for (int i = 0; i < items.length; i++) 
	        {
	            System.out.println(items[i] + ",");
	        }
	        System.out.println(items[items.length-1]);
	}
}