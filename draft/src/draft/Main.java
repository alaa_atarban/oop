package draft;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;

public class Main {

	public static void main(String[] args) {
		ArrayList<Student> students = new ArrayList<Student>();
		Student a = new Student("Alaa", "atarban", "12", "g1");
		Student b = new Student("omar", "something", "13", "g2");
		Student c = new Student("omar", "boo", "13", "g3");
		students.add(a);
		students.add(b);
		students.add(c);

		students.add(new Student("ahmed", "somthing", "14", "g3"));
		System.out.println(students);
		LinkedList<Student> students1 = new LinkedList<Student>();
		students1.add(new Student("Alaa2", "atarb", "121", "g2"));
		students1.add(new Student("fathi", "v", "122", "g1"));
		students1.add(new Student("Omar", "z", "123", "g1"));
		students1.add(new Student("Alaa", "y", "124", "g3"));
		students1.add(new Student("Ama", "c", "125", "g2"));
		HashSet<Student> AllStudents = new HashSet<Student>();
		AllStudents.addAll(students1);
		AllStudents.addAll(students);
		System.out.println(AllStudents);
		Student[] array = new Student[AllStudents.size()];
		AllStudents.toArray(array);
		for (int i = 0; i < array.length; i++) {
			System.out.println(array[i]);
		}
		System.out.println(Arrays.toString(array));
		Arrays.sort(array, new SortByNum());
		System.out.println("sorted " + Arrays.toString(array));

		Arrays.sort(array, new SortByName());
		System.out.println("sorted by last name " + Arrays.toString(array));
		List<Student> newstudents = Arrays.asList(array);
		ArrayList<Student> NewStudents = new ArrayList<Student>(newstudents);
		Student d = new Student("a", "b", "5", "g5");
		NewStudents.add(d);

		System.out.println("New arraylist " + NewStudents);

		NewStudents.removeIf(Student -> Student.getGbook().startsWith("1"));
		System.out.println("New arraylist with 1 removed " + NewStudents);
		HashMap<String, ArrayList<Student>> Student2 = new HashMap<String, ArrayList<Student>>();
		for (Student a1 : newstudents) {
			if (!Student2.containsKey(a1.GroupName)) {
				Student2.put(a1.GroupName, new ArrayList<Student>());
			}
			Student2.get(a1.GroupName).add(a1);
		}

		System.out.println("new hashmap" + Student2);
		TreeMap<String, ArrayList<Student>> sorted = new TreeMap<>(Collections.reverseOrder());
		sorted.putAll(Student2);
		System.out.println("sorted hashmap " + sorted);
		StudentContainer array1 = new StudentContainer();
		array1.add(a);
		array1.add(b);
		array1.add(c);
		array1.add(d);
		System.out.println(array1);

		array1.remove(2);
		System.out.println(array1);
		Student[] array2 = array1.toArray();
		System.out.println(Arrays.toString(array2));
		
		Iterator<Student> iterator = array1.iterator();
		while (iterator.hasNext()) {
			Student student = iterator.next();
			System.out.println(student);
		}

		for (Student student : array1) {
			System.out.println(student);
		}
		Iterator<Student> iterator1 = array1.iterator();
		while (iterator1.hasNext()) {
			iterator1.next();
			iterator1.remove();
		}
		System.out.println(array1.size()); 
		System.out.println(array1);

	}
}
