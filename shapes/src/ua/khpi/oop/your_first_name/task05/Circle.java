package ua.khpi.oop.your_first_name.task05;

public class Circle {
	public double radius;
	public double area;
	
	public Circle(double r)
	{
		this.radius = r;
	};
	public  double getArea() {
		area = 3.14 * radius * radius;
		return area;
	}

}
