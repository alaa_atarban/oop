package ua.khpi.oop.alaa.task09;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;



public class Main {

	public static void main(String[] args) {
		
		List<Circle> list = new ArrayList<Circle>();
		for(int i=0;i<10;i++)
		{
			double radius = 10 * Math.random();
			list.add(new Circle(radius));
			
		}
		for(int i=0;i<-1;i++)
		{
			double radius = 10 * Math.random();
			if (Math.random()<= 0.5)
				i = -1;
			list.add(new Circle(radius));
			
		}
		TreeMap<String, String>  map = new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER);
		System.out.println(list);
		map.put("a1234", "Steve Jobs\r\n"
				+ "");
		map.put("a1235", "Scott McNealy\r\n"
				+ "");
		map.put("a1236", "Jeff Bezos\r\n"
				+ "");
		map.put("a1237", "Larry Ellison\r\n"
				+ "");
		map.put("a1238", "Bill Gates\r\n"
				+ "");
		System.out.println(map.get("A1238"));
		System.out.println(map);
		//extra:
		List<Circle> arrayList = new ArrayList<Circle>();
		arrayList.add(new Circle(3));
		arrayList.add(new Circle(2));
		arrayList.add(new Circle(1));
		System.out.println(arrayList);
		MyComparator myComparator = new MyComparator();
		arrayList.sort(myComparator);
		System.out.println(arrayList);
	}

}
