package ua.khpi.oop.your_first_name.task04;

class One {
	 int x;

	One() {
		this(999);
		System.out.println("One::One()");
		
	}

	One(int x) {
		this.x= x;
		System.out.println("One::One(int)");
		
	}
;
	public One getInstance(int x) {
		
		return this;
	}
}
	
