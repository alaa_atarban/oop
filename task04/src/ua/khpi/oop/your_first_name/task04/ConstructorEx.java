package ua.khpi.oop.your_first_name.task04;

import java.util.Arrays;

public class ConstructorEx {
	ConstructorEx(int length) {
		// TODO: Initialize the field "array" by creating a new array using the
		//       specified "length".
		array = new int[length];
	}

	ConstructorEx(int[] srcArray) {
		// TODO: Initialize the "array" field with the reference
		//       to the specified "srcArray".
		array = srcArray;
	}

	ConstructorEx(int[] srcArray, boolean copyArray) {
		// TODO: Initialize the "array" field by reference to the specified "srcArray"
		//       or by copying the contents of the "srcArray" depending on the "copyArray" flag.
		if(copyArray) {
			array = Arrays.copyOfRange(srcArray, 0, srcArray.length);

			 
		}else {
			array = srcArray;
		}
	}
	int[] array;

}
