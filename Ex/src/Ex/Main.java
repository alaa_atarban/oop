package Ex;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import thread.HelloRunnable;

public class Main {

	public static void main(String[] args) throws InterruptedException {
		ExecutorService executor = Executors.newFixedThreadPool(10);
		Runnable task = new Mytask(1000,20);
	

		executor.execute(task);
		executor.shutdown();
		do {
			System.out.println(Thread.currentThread().getName() + ": waiting...");
			executor.awaitTermination(10, TimeUnit.SECONDS);
		} while (!executor.isTerminated());

	}

}
