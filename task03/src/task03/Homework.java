package task03;

public class Homework {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(sum(23,1));
		System.out.println(sum(5,1,1));
		System.out.println(div(7,5));
		System.out.println(div(7.0,5));
		}
	public static int sum(int a,int b) {
		int sum = a+b;
		return sum;
		
	}
	public static int sum(int a,int b,int c) {
		int sum = a+b+c;
		return sum;
		
	}
	public static int div(int a,int b) {
		int res = a/b;
		return res;
		
	}
	public static double div(double a,double b) {
		double res = a/b;
		return res;
		
	}
}
