package task03;

public class One {
	public static int Square(int x)
	{
		// Calculate and return the square of 'x'.
		x=x*x;
		return x;
	}
	public static int power(int x,int y)
	{
		// Return the result of 'x' raised to 'y'th power.
		int p = x;
		for(int i=1;i<y; i++)
			p = p * x;
		return p;
	}
	public static void power(A a,int y) {
		// Raise 'a.x' to the 'y'th power.
		for(int i =1;i <y; i++)
			a.x = a.x * a.x;
	
	}
	public static void power(int[] array, int y) {
		// Raise each element of an array to the 'y'th power.
		for(int i=0;i<array.length;i++)
		{
			for(int j=1;j<y; j++) {
				array[i] = array[i] * array[i];
				}
		}
	}

}
