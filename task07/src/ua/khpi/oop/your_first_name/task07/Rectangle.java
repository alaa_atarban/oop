package ua.khpi.oop.your_first_name.task07;

public class Rectangle extends shapes {

	private double width, height;

	public Rectangle(double width, double height) {
		this.width = width;
		this.height = height;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}
	@Override
	public double getArea() {
		return width * height;
	}

	public String toString() {
		return "Rectangle: width = " + getWidth() + ", height = " + getHeight();
	}
}
