package ua.khpi.oop.your_first_name.task07;

public class ShapeUtils {
	public static void printShapes(shapes[] shapes) {
		for (shapes s : shapes) {
			System.out.println(s);
		}
	}
	public static double sumAreas(shapes[] shape) {
		
		double sum = 0;
		for (shapes s : shape) {
			// sum = sum + s.getArea(); // Illegal! Object does not have getArea
			
			sum = sum +  s.getArea();
		
		}
		return sum;
	}
}