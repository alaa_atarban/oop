package ua.khpi.oop.your_first_name.task07;

public class Main {

	public static void main(String[] args) {
		shapes[] shapes = { new Circle(10), new Rectangle(20, 30), new Square(40) };
		ShapeUtils.printShapes(shapes);
		System.out.println();
		Double sum = ShapeUtils.sumAreas(shapes);
		System.out.format("%.2f", sum);
	}
	

}
