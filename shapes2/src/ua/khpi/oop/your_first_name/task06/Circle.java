package ua.khpi.oop.your_first_name.task06;

public class Circle {
	private double radius;
	private double area;
	
	public Circle(double r)
	{
		this.radius = r;
	};
	public  double getArea() {
		area = 3.14 * radius * radius;
		return area;
	}
	public double getRadius() {
		return radius;
	}
	public void setRadius(double radius) {
		this.radius = radius;
	}
	public void setArea(double area) {
		this.area = area;
	}
	public String toString() {
		return "rauis = "+ radius + " area = " + area;
	}


}
