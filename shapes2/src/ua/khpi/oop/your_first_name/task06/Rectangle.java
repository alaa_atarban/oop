package ua.khpi.oop.your_first_name.task06;

public class Rectangle {
	private double width;
	private double  height;
	private double area;
	
	


	Rectangle(double height, double width) {
		this.height = height;
		this.width = width;
	}

	double getHeight() {
		return height;
	}

	double getWidth() {
		return width;
	}

	void setHeight(double h) {
		height = h;
	}

	void setWidth(double w) {
		width = w;
	}

	

	public double getArea() {
		 this.area = height * width;
		 return area;
	}
	public void setArea(double a) {
		this.area=a;
		this.width = Math.sqrt(area);
		this.height = Math.sqrt(area);
		
	}
	public String toString() {
		return "width = " + width + ", height =  " + height+ ", area = " + area;
	}

}
