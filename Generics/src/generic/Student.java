package generic;



public class Student {
String FirstName;
String LastName;
String gbook;
String GroupName;
Student(String FirstName, String LastName, String gbook, String GroupName)
{
this.FirstName =FirstName;
this.LastName = LastName;
this.gbook=gbook;
this.GroupName = GroupName;

}
@Override
public String toString() {
return "Student [FirstName=" + FirstName + ", LastName=" + LastName + ", gbook=" + gbook + ", GroupName="
+ GroupName + "]";
}
public String getFirstName() {
return FirstName;
}
public void setFirstName(String firstName) {
FirstName = firstName;
}
public String getLastName() {
return LastName;
}
public void setLastName(String lastName) {
LastName = lastName;
}
public String getGbook() {
return gbook;
}
public void setGbook(String gbook) {
this.gbook = gbook;
}
public String getGroupName() {
return GroupName;
}
public void setGroupName(String groupName) {
GroupName = groupName;
}
@Override
public int hashCode() {
final int prime = 31;
int result = 1;
result = prime * result + ((FirstName == null) ? 0 : FirstName.hashCode());
result = prime * result + ((GroupName == null) ? 0 : GroupName.hashCode());
result = prime * result + ((LastName == null) ? 0 : LastName.hashCode());
result = prime * result + ((gbook == null) ? 0 : gbook.hashCode());
return result;
}
@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
if (obj == null)
return false;
if (getClass() != obj.getClass())
return false;
Student other = (Student) obj;
if (FirstName == null) {
if (other.FirstName != null)
return false;
} else if (!FirstName.equals(other.FirstName))
return false;
if (GroupName == null) {
if (other.GroupName != null)
return false;
} else if (!GroupName.equals(other.GroupName))
return false;
if (LastName == null) {
if (other.LastName != null)
return false;
} else if (!LastName.equals(other.LastName))
return false;
if (gbook == null) {
if (other.gbook != null)
return false;
} else if (!gbook.equals(other.gbook))
return false;
return true;
}

}

