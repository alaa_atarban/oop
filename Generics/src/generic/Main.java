package generic;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;



public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Container<Student> container = new Container<>();
		container.add(new Student("fathi", "v", "122", "g1"));
		container.add(new Student("Omar", "z", "123", "g1"));
		container.add(new Student("Alaa", "y", "124", "g3"));
		container.add(new Student("Ama", "c", "125", "g2"));
		System.out.println(container);
		System.out.println(container.set(1,new Student("Alaa2", "atarb", "121", "g2")));
		System.out.println(container);
		System.out.println(container.remove(2));
		System.out.println(container);
		container.remove(0);
		System.out.println(container);
		Container<String> str = new Container<>();
		str.add("ccc");
		str.add("bbb");
		str.add("aaa");
		System.out.println(str);
		System.out.println(str.set(0, "zzz"));
		System.out.println(str);
		System.out.println(str.remove(2));
		System.out.println(str);
		Container<One> one = new Container<>();
		one.add(new One(1));
		one.add(new One(2));
		one.add(new One(3));
		System.out.println(one);
		System.out.println(one.set(1, new One(999)));
		System.out.println(one);
		System.out.println(one.remove(2));
		System.out.println(one);
		Container<String> str1 = new Container<>();
		
		str1.addAll(Arrays.asList("ddd", "fff", "eee"));
		System.out.println(str1);
		
		Container<One> one1 = new Container<>();
		Container<Student> stud = new Container<>();

		one1.addAll(Arrays.asList(new One(222), new One(777), new One(555)));
		System.out.println(one1);
		stud.addAll(Arrays.asList(new Student("fathi", "v", "122", "g1"), new Student("mo", "v", "112", "g2"), new Student("alaa", "v", "111", "g3")));
		System.out.println(stud);
		one.sort(new Comparator<One>() {
			
			@Override
			public int compare(One arg0, One arg1) {
				// TODO Auto-generated method stub
				return arg1.i-arg0.i;
			}
		});
		System.out.println(one1);
		Container<One> one2 = new Container<>();
		
		System.out.println(one2);
		Iterator<One> iterator = one.iterator();
		while (iterator.hasNext()) {
			One x = iterator.next();
			System.out.println(x);
		}
		
		iterator = one2.iterator();
		while (iterator.hasNext()) {
			iterator.next();
			iterator.remove();
		}
		System.out.println(one2.size());
		System.out.println(one2);
		
		Container<Integer> intContainer = new Container<>();
		intContainer.addAll(
			Arrays.asList(
				Integer.valueOf(0xFF),
				Integer.valueOf(0x121),
				Integer.valueOf(0xE00E),
				Integer.valueOf(0xE0E0),
				Integer.valueOf(0x0E0)
			)
		);
		System.out.println(intContainer);
		intContainer.remove(0xFF);
		intContainer.remove(0x121);
		intContainer.remove(0xE00E);
		System.out.println(intContainer);
	}

}
