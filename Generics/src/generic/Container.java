package generic;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

class Container<E>  implements Iterable<E> {
	private Object[] array = new Object[0];

	void add(E element) {
		// **********************************************
		Object[] old = array;
		array = Arrays.copyOf(old, old.length+1);
		// **********************************************
		// Use Arrays.copyOf() instead of the above code.
		// **********************************************
		array[old.length] = element;
	}

	@Override
	public  String toString() {
		return Arrays.toString(array);
	}
	/**
	 * Returns the number of elements in this list.
	 *
	 * @return the number of elements in this list
	 */
	public int size() {
		// TODO: Add your implementation here.
		return array.length;
	}

	/**
	 * Removes all of the elements from this container. The container will be empty
	 * after this call returns.
	 */
	public void clear() {
		array = new Object[0];
	}

	/**
	 * Returns the element at the specified position in this container.
	 *
	 * @param index index of the element to return
	 * @return the element at the specified position in this container
	 */
	@SuppressWarnings("unchecked")
	public E get(int index) {
		// TODO: Add your implementation here.
		return (E) array[index];
	}

	/**
	 * Replaces the element at the specified position in this container with the
	 * specified element.
	 *
	 * @param index   index of the element to replace
	 * @param element element to be stored at the specified position
	 * @return the element previously at the specified position
	 */
	@SuppressWarnings("unchecked")
	public E set(int index, E element) {
		E a = (E) array[index];
		array[index]= element;
		
		return a;
	}

	/**
	 * Removes the element at the specified position in this container. Shifts any
	 * subsequent elements to the left (subtracts one from their indices).
	 *
	 * @param index the index of the element to be removed
	 * @return the element that was removed from the list
	 */
	public Object remove(int index) {
		Object[] remove = new Object[array.length-1];
		 
		for(int i = 0,  j = 0;  i<array.length;i++)
		{
			if(i != index)
			{
				remove[j++] = array[i];
			}
		}

		Object x = array[index];
		array = remove;
		
		return x;
	}
	public boolean addAll(Collection<? extends E> c) {
		Object[] a = c.toArray();
		if (a.length == 0) {
			return false;
		}
		// Add your implementation here.
		// Hint: It is convenient to use System.arraycopy()
		// ...
		Object[] arr = new Object[a.length];
		System.arraycopy(a, 0, arr,0, a.length);
		array = Arrays.copyOf(arr, arr.length);
		
		return true;
	}
	@SuppressWarnings("unchecked")
	void sort(Comparator<E> c) {
		// Here you can use the sorting method from the Java library.
		Arrays.sort((E[])array,c);
		
	}
	@Override
	public Iterator<E> iterator() {
		// TODO Auto-generated method stub
		return new SimpleIterator();
	}
	
	
	private class SimpleIterator implements Iterator<E> {
		// Add some fields here if needed.
		 int i = 0;
		  int lastRead = -1;
		public boolean hasNext() {
			 return i < size();
		}
		public E next() {
			 if (!hasNext()) {
		          throw new NoSuchElementException();
		        }
			return get(lastRead = i++);
		}
		public void remove() {
			Container.this.remove(lastRead);
	        i--;
	        lastRead = -1;
		}
	
		      }
	}



